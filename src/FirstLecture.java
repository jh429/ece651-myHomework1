import people.Instructor;
import people.TeachAssistant;
import people.Person;
import people.Student;

public class FirstLecture {
	public static void main(String args[]) {
		Person[] Participants = new Person[6];
		Participants[0] = new Instructor("Ric", "ECE-651");
		Participants[1] = new TeachAssistant("Lee");
		Participants[2] = new TeachAssistant("Teddy");
		Participants[3] = new TeachAssistant("TC");
		Participants[4] = new Student("Jingxiong", "Python");
		Participants[5] = new Student("Xin", "C++");

		for (Person p : Participants) {
			if (p != null) {
				p.selfIntroduction();
			}
		}		
		
	}
	
}
