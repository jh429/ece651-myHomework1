package Skyline;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Comparator;


public class SkylineDrawer {
	static public List<int[]> getSkyline(Building[] buildings) {
        List<int[]> res = new ArrayList<int[]>();
        PriorityQueue<Building> inView = new PriorityQueue<Building>(new Comparator<Building>(){
            public int compare(Building a, Building b){
                if (a.height != b.height) {
                    return b.height - a.height;
                }
                return b.rIndex - a.rIndex;
            }
        });
        int i = 0, currX, currH, len = buildings.length;
        while (i < len || !inView.isEmpty()) {
            if (inView.isEmpty() || i < len && buildings[i].lIndex <= inView.peek().rIndex) {
                currX = buildings[i].lIndex;
                while (i < len && currX == buildings[i].lIndex) {
                    inView.offer(new Building(buildings[i].lIndex, buildings[i].rIndex, buildings[i].height));
                    i++;
                }
            }
            else {
                currX = inView.peek().rIndex;
                while (!inView.isEmpty() && currX >= inView.peek().rIndex) {
                    inView.poll();
                }
            }
            currH = inView.isEmpty() ? 0 : inView.peek().height;
            if (res.isEmpty() || res.get(res.size() - 1)[1] != currH) {
                res.add(new int[]{currX, currH});
            }
        }
        return res;
    }
}


