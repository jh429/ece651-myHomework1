package Skyline;

public class Building {
	public int lIndex;
	public int rIndex;
	public int height;
	public Building(int l, int r, int h) {
		this.lIndex = l;
		this.rIndex = r;
		this.height = h;
	}
}
