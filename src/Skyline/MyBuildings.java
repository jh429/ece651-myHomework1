package Skyline;

public class MyBuildings {
	static Building b1 = new Building(2, 9, 10);
	static Building b2 = new Building(3, 7, 15);
	static Building b3 = new Building(5, 12, 12);
	static Building b4 = new Building(15, 20, 10);
	static Building b5 = new Building(19, 24, 8);
	public static Building[] buildings = new Building[]{b1, b2, b3, b4, b5};
}
