import Skyline.SkylineDrawer;
import Skyline.Building;
import Skyline.MyBuildings;
import java.util.List;

public class Hello {
	public static void main(String args[]) {
		System.out.println("Hello world!!\nWe have the following buildings:");
		for (Building b: MyBuildings.buildings) {
			System.out.println("left index: " + b.lIndex + " right index: " + b.rIndex + " height: " + b.height);
		}
		List<int[]> res = SkylineDrawer.getSkyline(MyBuildings.buildings);
		System.out.println("Buildings skyling:");
		for (int[] point: res) {
			System.out.println("horizontal index: " + point[0] + ", vertical index: " + point[1]);
		}
		
	}
}
