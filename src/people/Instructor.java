package people;

public class Instructor extends Person{
	String className;
	public Instructor(String name) {
		super(name);
	}
	
	public Instructor(String name, String className) {
		super(name);
		this.className = className;
	}
	
	@Override
	public void selfIntroduction() {
		System.out.println("I am " + this.getName() + ", instructor of " + this.className);		
	}
	
}
