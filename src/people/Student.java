package people;

public class Student extends Person{
	String language;
	public Student(String name, String lang) {
		super(name);
		this.language = lang;
	}

	@Override
	public void selfIntroduction() {
		System.out.println("I am " + this.getName() + ", I like " + this.language + ".");		
	}

}
