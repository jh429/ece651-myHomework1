package people;

public class TeachAssistant extends Person{
	
	public TeachAssistant(String name) {
		super(name);
	}

	@Override
	public void selfIntroduction() {
		System.out.println("I am " + this.getName() + ", TA of this class.");		
	}

}
